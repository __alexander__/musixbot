# MUSIXBOT

Bot para Facebook Messenger al que se le puede enviar parte de la letra de una canción para que nos retorne la canción a la que pertenece.

## Características

* Búsqueda del nombre de una canción en base a la letra
* Opción de guardar la canción como favorita
* Listado de Favoritos
* Estadísticas del bot como "número de usuarios" y las "canciones más populares".

## Uso

El listado de instrucciones que se pueden usar en el chat es el siguiente:

* Búsqueda de una canción:

    **/buscar** <letra de la canción>
 
* Marcar una canción como favorita: se debe dar click en alguno de los resultados.

* Mostrar la lista de favoritos

    **/favoritos**
    
## Estadísticas del bot
 
Se debe acceder al subdirectorio: **/estadisticas/**
 
## Algunas capturas de pantalla
 
**Búsqueda**
 
![Buscar canciones](screenshots/1_buscar.png)
 
**Marcar resultado como favorito**
 
![Marcar favorito](screenshots/2_guardar.png)
  
**Mostrar favoritos**

![Mostrar favoritos](screenshots/3_favoritos.png)

**Estadísticas de la aplicación**

![Mostrar estadísticas](screenshots/4_estadisticas.png)

## ToDo

Revisar la lista de Issues para la lista de características pendientes a implementar.