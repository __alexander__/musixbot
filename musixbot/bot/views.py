import json

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.db.models import Count
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from pymessenger.bot import Bot

from .bot import Musixbot
from .models import UserSong

FB_ACCESS_TOKEN = settings.ENV.get('FB_ACCESS_TOKEN')
FB_VERIFY_TOKEN = settings.ENV.get('FB_VERIFY_TOKEN')
bot = Bot(FB_ACCESS_TOKEN)


@csrf_exempt
def callback(request):
    if request.method == 'GET':
        ''' Check token '''
        token_sent = request.GET.get('hub.verify_token')
        return verify_fb_token(request, token_sent)
    else:
        output = json.loads(request.body.decode('utf-8'))
        for event in output['entry']:
            messaging = event['messaging']
            for message in messaging:
                if message.get('message'):
                    recipient_id = message['sender']['id']
                    musixbot = Musixbot(recipient_id)
                    text_from_user = message['message'].get('text')
                    if message['message'].get('quick_reply'):
                        print(':: quick_reply ::')
                        quick_reply = message['message'].get('quick_reply')
                        # print(quick_reply['payload'])
                        musixbot.check_action(quick_reply['payload'])
                    elif text_from_user:
                        musixbot.check_action(text_from_user)
                    else:
                        musixbot.send_default_message()

    return HttpResponse('Message Processed')


@login_required()
def stats(request):
    number_of_top_songs = 5
    # total_users = UserSong.objects.distinct('user_id').count()  # distinct, not supported on sqlite3
    total_users = UserSong.objects.values('user_id').annotate(Count('user_id')).count()
    most_popular_songs = UserSong.objects.values('track_id', 'track_name').annotate(Count('track_id')).order_by(
        '-track_id__count')[:number_of_top_songs]

    return render(request, 'bot/stats.html', locals())


def verify_fb_token(request, token_sent):
    """ Verify the token from facebook """
    if token_sent == FB_VERIFY_TOKEN:
        return HttpResponse(request.GET.get('hub.challenge'))
    return HttpResponse('Invalid verification token')
