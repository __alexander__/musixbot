from django.urls import path

from . import views

urlpatterns = [
    path('callback/', views.callback, name='callback'),
    path('estadisticas/', views.stats, name='stats'),
]
