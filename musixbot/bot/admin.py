from django.contrib import admin

from .models import UserSong


@admin.register(UserSong)
class UserSongAdmin(admin.ModelAdmin):
    list_display = ['user_id', 'track_name', 'artist_name']
