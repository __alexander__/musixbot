import json

from django.conf import settings
from pymessenger.bot import Bot

from musixmatch.api import search_song
from .models import UserSong

COMMAND_SEARCH = '/buscar'
COMMAND_SAVE = '/guardar'
COMMAND_SAVED = '/favoritos'
DEFAULT_TEXT = 'Para buscar una canción, escriba {} y luego parte de la letra de la canción'.format(COMMAND_SEARCH)

FB_ACCESS_TOKEN = settings.ENV.get('FB_ACCESS_TOKEN')

bot = Bot(FB_ACCESS_TOKEN)


class Musixbot:

    def __init__(self, recipient_id):
        self.recipient_id = recipient_id

    def check_action(self, text_from_user):
        if text_from_user:
            if text_from_user.startswith(COMMAND_SEARCH):
                songs = search_song(text_from_user.replace(COMMAND_SEARCH, ''))
                self.send_songs(songs)
            elif text_from_user.startswith(COMMAND_SAVED):
                return self.show_saved()
            elif text_from_user.startswith(COMMAND_SAVE):
                self.save_song(text_from_user)
            else:
                self.send_default_message()
        else:
            self.send_default_message()

    def send_songs(self, songs):
        print('send_songs')
        song_quick_replies = []
        for song in songs:
            track = song['track']
            song_quick_replies.append({
                'content_type': 'text',
                'title': track.get('track_name')[:20],
                'payload': '{} {}'.format(COMMAND_SAVE, json.dumps({
                    'track_id': track['track_id'],
                    'track_name': track['track_name'],
                    'artist_name': track['artist_name']
                })),
            })
        payload = {
            'recipient': {
                'id': self.recipient_id
            },
            'message': {
                "text": 'Click en una canción para marcarla como favorita:',
                'quick_replies': song_quick_replies
            }
        }
        bot.send_raw(payload)

    def show_saved(self):
        # print('Canciones guardadas')
        text_songs = ''
        songs = UserSong.objects.filter(user_id=self.recipient_id).values_list('track_name', 'artist_name')
        for num, song in enumerate(songs):
            text_songs = text_songs + '{}. {} - {}\n'.format(num + 1, song[0], song[1])
        bot.send_text_message(self.recipient_id, 'Canciones guardadas\n' + text_songs)

    def send_default_message(self):
        print(DEFAULT_TEXT)
        bot.send_text_message(self.recipient_id, DEFAULT_TEXT)

    def save_song(self, text_from_user):
        song = json.loads(text_from_user.replace(COMMAND_SAVE, '').strip())
        UserSong.objects.get_or_create(user_id=self.recipient_id, track_id=song['track_id'], defaults={
            'track_name': song['track_name'],
            'artist_name': song['artist_name']
        })
        bot.send_text_message(self.recipient_id,
                              'La canción se marcó como favorita.\n Para mostrar sus favoritos escriba /favoritos')
