from django.db import models


class UserSong(models.Model):
    user_id = models.CharField(max_length=32, db_index=True, help_text='Facebook user ID')
    track_id = models.CharField(max_length=16)
    track_name = models.CharField(max_length=255)
    artist_name = models.CharField(max_length=255)

    def __str__(self):
        return self.track_name
