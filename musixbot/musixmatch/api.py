import json
from urllib import parse

import faster_than_requests as requests
from django.conf import settings

MUSIXMATCH_API_KEY = settings.ENV.get('MUSIXMATCH_API_KEY')


def search_song(q):
    """ Search for songs through the musixmatch API """
    params = (
        ('format', 'jsonp'),
        ('callback', 'callback'),
        ('q_lyrics', q),
        ('quorum_factor', '1'),
        ('apikey', MUSIXMATCH_API_KEY),
    )
    query_params = parse.urlencode(params)
    requests.set_headers([('Accept', 'text/plain')])
    response = requests.get('https://api.musixmatch.com/ws/1.1/track.search?{}'.format(query_params))
    json_response = json.loads(response['body'][9:-2])  # [9:-2]: remove the callback format
    songs = json_response['message']['body']['track_list']
    # print(songs)
    # print('='*20)
    return songs